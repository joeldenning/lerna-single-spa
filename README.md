# `single-spa` inside `lerna` monorepo experiment

- React + typescript
- React + javascript
- Using systemJs to import react, react-dom,... for all packages

===

### Optimize bundle by using

1. Importer

- babel-plugin-lodash
- babel-plugin-ramda

So you can import 'lodash' or 'ramda' without fear bundle size. It will cherry-pick to Lodash modules.

2. Library

   Using systemJs to add third-party library. Modules can be used by that third-party library.
   Using only one node_modules directory to reduce duplicated library.
   `react`, `react-dom`, `single-spa-react` (see at config/libMap.js) are separated independently

3. Lazy loading

   Support lazy loading and chunk js.

## Run:

- Install the dependencies
   - `$ yarn`
- Run the app
   - `$ yarn start`
- Browse to http://localhost:9200.
- Build the app
   - `$ yarn build`



## Build commands

- `$ yarn build`
- `$ yarn build:portal`

You can build each module by using lerna.

## Todo

[] Support PWA
[] Support cache for PWA
[] Lazy loading example (Using React.lazy or SystemJS)
[x] Install npm library instead cdn
[x] Hot module reloading for new SPAs (See app1, appTs)
[x] Use Externals package to build common packages to share dynamically between SPAs (see packages/import-map)
[x] Don't need to run all SPAs to work on one
[x] Load SPA's on demand with SystemJS and Single-Spa
