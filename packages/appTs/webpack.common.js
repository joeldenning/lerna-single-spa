const { modulesConfig } = require("../../config/webpack/config");
const {
  loadTs,
  loadJs,
  loadCSS,
  loadSCSS,
  loadFile
} = require("../../config/webpack/webpack.module");
const appConfig = modulesConfig["appTs"];
const merge = require("webpack-merge");

const loadModuleRules = ({ publicPath }) => [
  loadTs(),
  loadJs(),
  loadSCSS(),
  // loadCSS(),
  loadFile({ publicPath })
];

const buildCommonConfig = config =>
  merge([
    ...loadModuleRules(config),
    {
      entry: "./src/index.tsx",
      resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        modules: [__dirname, "node_modules"],
        alias: {
          // "@common": path.resolve(__dirname, "../common/src")
        }
      }
    }
  ]);

module.exports = {
  buildCommonConfig,
  appConfig
};
