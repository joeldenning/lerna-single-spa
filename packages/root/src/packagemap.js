// const { peerDependencies } = require('../../../config/webpack/packageMap')
// const packageMap = {
//   import: {
//     react: 'https://cdnjs.cloudflare.com/ajax/libs/react/16.8.6/umd/react.development.js',
//     'react-dom': 'https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.8.6/umd/react-dom.development.js',
//     'react-dom/server': 'https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.8.6/umd/react-dom-server.browser.development.js',
//     'single-spa': 'https://unpkg.com/single-spa@4.3.2/lib/umd/single-spa.min.js',
//     lodash: 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js',
//     rxjs: 'https://unpkg.com/rxjs@6.4.0/bundles/rxjs.umd.js',
//   }
// }
// {
//   "scopes": {
//     "/lib": {
//       "packages": {
//         "react": "/lib/react/react@16.6.0.js",
//           "react-dom": "/lib/react-dom/react-dom@16.6.0.js"
//       }
//     },
//     "/lib/components": {
//       "packages": {
//         "react": "/lib/react/react@16.6.0.js",
//           "react-dom": "/lib/react-dom/react-dom@16.6.0.js"
//       }
//     },
//     "/packages/navigation": {
//       "packages": {
//         "react": "/lib/react/react@16.6.0.js",
//           "react-dom": "/lib/react-dom/react-dom@16.6.0.js",
//             "single-spa-react": "/lib/single-spa-react/single-spa-react@2.8.1.js",
//               "react-router-dom": "/lib/react-router-dom/react-router-dom@4.3.1.js",
//                 "components": "/lib/components/components@1.0.0.js"
//       }
//     },
//     "/packages/app1": {
//       "packages": {
//         "react": "/lib/react/react@16.6.0.js",
//           "react-dom": "/lib/react-dom/react-dom@16.6.0.js",
//             "single-spa-react": "/lib/single-spa-react/single-spa-react@2.8.1.js",
//               "react-router-dom": "/lib/react-router-dom/react-router-dom@4.3.1.js",
//                 "components": "/lib/components/components@1.0.0.js"
//       }
//     },
//     "/packages/app2": {
//       "packages": {
//         "react": "/lib/react/react@16.6.0.js",
//           "react-dom": "/lib/react-dom/react-dom@16.6.0.js",
//             "single-spa-react": "/lib/single-spa-react/single-spa-react@2.8.1.js",
//               "react-router-dom": "/lib/react-router-dom/react-router-dom@4.3.1.js",
//                 "components": "/lib/components/components@1.0.0.js"
//       }
//     }
//   }
// }

// const buildImportMapJson = () => {
//   const packageImprotMapJson = Object.entries(peerDependencies).reduce((packageMapJson, [packageName, lib]) => {
//     const packagePath = `/packages/${packageName}`
//     return {
//       ...packageMapJson,
//       [packagePath]:  lib
//     }
//   }, {})
//   return {
//     scopes: packageImprotMapJson
//   }
// }

// console.log(buildImportMapJson())
const insertExternalImportMap = (newMapJSON) => {
  const newScript = document.createElement('script')
  newScript.type = 'systemjs-importmap'
  newScript.text = JSON.stringify(newMapJSON)

  document.head.appendChild(newScript)
}

const insertPackageMap = () => {
  insertExternalImportMap({
    scopes:
    {
      '/packages/app1':
      {
        react: '/lib/react@16.8.6.js',
        'react-dom': '/lib/react-dom@16.8.6.js',
        redux: '/lib/redux@4.0.1.js',
        'single-spa-react': '/lib/single-spa-react@2.10.2.js'
      },
      '/packages/appTs':
      {
        react: '/lib/react@16.8.6.js',
        'react-dom': '/lib/react-dom@16.8.6.js',
        redux: '/lib/redux@3.7.2.js',
        'single-spa-react': '/lib/single-spa-react@2.10.2.js'
      }
    }
  })
}


module.exports = {
  insertExternalImportMap,
  // packageMap,
  insertPackageMap
}